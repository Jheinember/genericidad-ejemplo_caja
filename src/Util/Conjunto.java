/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i>=this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i]=new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar()
    {
        //
        for (int j = 0; j < i-1; j++) {
            T menor=this.cajas[j].getObjeto();
            int idx=j;
        for(int k=j+1;k<i;k++){
        Comparable comparador=(Comparable)menor;
        T dato_A_comparar=this.cajas[k].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo>0){
            System.out.println("menor "+cajas[k].getObjeto().toString()+" con it "+j);
            menor=dato_A_comparar;
            idx=k;
        }
        }
        cajas[idx]=cajas[j];
        cajas[j]=new Caja<>(menor);
            System.out.println("Cambie: "+cajas[idx].getObjeto().toString()+" con: "+cajas[j].getObjeto().toString());
        }
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja()
    {
        for (int j = 0; j < i-1; j++) {                   
            for (int k = 0; k < (i-j)-1; k++) {
          Comparable comparador=(Comparable)cajas[k].getObjeto(); 
        T dato_A_comparar=this.cajas[k+1].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo>0){
            cajas[k+1]=cajas[k];
            cajas[k]=new Caja<>(dato_A_comparar);
            //menor=dato_A_comparar;
            //idx=k;  
            }
 
        }
    
    }
    }
    
    /**
     * Elimina un elemento del conjunto y compacta
     * @param objBorrado es el objeto que deseo eliminar
     */
    
    public void remover(T objBorrado)
    {int j=indexOf(objBorrado);    
       if(j==-1)throw new RuntimeException("No se encontró el elemento que desea remover.");
       while(j<i-1){
           cajas[j]=cajas[j+1];
           j++;
       }
       cajas[i-1]=null;
       i--;
    }
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo) throws Exception
    {
        Conjunto<T> temp=new Conjunto<>(i+nuevo.getCapacidad());
        
        for (int j = 0; j < i; j++) {
            temp.adicionarElemento(cajas[j].getObjeto());
        }
        for (int j = 0; j <nuevo.getCapacidad(); j++) {
            temp.cajas[i]=nuevo.cajas[j];
            i++;
        }
        cajas=temp.cajas;
        i=temp.getCapacidad();
     nuevo.removeAll();
    }
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso SI toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo) throws Exception
    {
        Caja<T> []temp=this.cajas;
        this.cajas=new Caja[getCapacidad()+nuevo.getCapacidad()];
        i=0;
        for (int j = 0; j < temp.length; j++) {
            cajas[j]=temp[j];
            i++;
        }
     for (int j = 0; j < nuevo.getCapacidad(); j++) {
            if(!existeElemento(nuevo.get(j)))adicionarElemento(nuevo.get(j));
        }
       nuevo.removeAll();
    }
    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
      
        for(Caja c:this.cajas)
            if(c!=null)
            msg+=c.getObjeto().toString()+" --> ";
            else{
                msg+="    --> ";
            }
        return msg;
    }
    
    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int i=1;i<this.getCapacidad();i++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[i].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }
}
