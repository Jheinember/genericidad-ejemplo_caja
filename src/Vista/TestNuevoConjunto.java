/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author JHEINEMBER
 */
public class TestNuevoConjunto {
    
    public static void main(String[] args) throws Exception {
        
        Conjunto<Integer> c1 = new Conjunto(9);
        Conjunto<Integer> c2 = new Conjunto(5);
        Conjunto<Integer> c3 = new Conjunto(4);
        for (int i = 0; i < 9; i++) {
            c1.adicionarElemento((i * 512) % 50);
        }
        System.out.println(c1.toString());

        c1.ordenar();
        // c1.ordenarBurbuja();   
        System.out.println("Ordenado : \n");
        System.out.println(c1.toString());
        int t = (8 * 512) % 50;
        c1.remover(t);
        System.out.println(" al remover " + t);

        System.out.println(c1.toString());
        for (int i = 0; i < 4; i++) {
            c3.adicionarElemento(i);
        }
        System.out.println(" c3 : " + c3.toString());
        for (int i = 2; i < 7; i++) {
            c2.adicionarElemento(i);
        }
        System.out.println(" c2 : " + c2.toString());
        //c3.concatenar(c2);
        //System.out.println(" c3 concatena a c2");
        c3.concatenarRestrictivo(c2);
        System.out.println(" c3 concatenarestrictivamente a c2");
        System.out.println(c3.toString());
        
        
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.
        
    }
    
}

